/*
 * evelyn, STM32 bare-minimal. 
 * Copyright (c) 2017 - 2025
 * All Rights Reserved.
 * 
 * Hardik Madaan
 *
 * cn:      Brian doofus
 * email:   hardik.mad@gmail.com
 * web:     www.tachymoron.wordpress.com
 * git:     www.github.com/pundoobvi
 * bitbuck: https://bitbucket.org/pundoobvi/
 */
/**
  ******************************************************************************
  * @file           : main
  * @brief          : main
  * @author					: brian doofus
  * @project				: evelyn
  * @purpose				: just for fun
  ******************************************************************************
 * */

/* 						BRIEF
*  User sends a no 0-100 from PC-> USB Output endpoint(output with respect to the HOST) setup, report size, call read api <- duty cycle
*  CRC 16 check of all the data on USB.
*  Send acknowledgement to PC setup USB Input endpoint, report size, call send api.
*  DMA, is it even req?
*  
*  Blink an led, for total 10s with duty cycle which user sent.
*  if duty cycle % 4 == 0 send Nymble, blink led
*  if duty cycle % 4 == 0 send Labs, blink led
*  if duty cycle % 4 && % 7  == 0 send NymbleLabs, blink led
*	 if duty cycle % 4 && % 7  != 0 send duty cycle and dont blink led
*
*	 duty cycle to be stored in a queue, then handled
*
*	 code to send from Computer(linux machine) to controller
*/

#include "stm32f4xx.h"
#include "usb_device.h"
#include "usbd_customhid.h"

#include "main.h"
#include "evelyn.h"
#include "evelynconfig.h"
#include "cbuffer.h"

/* Global decalrations */
/* Circular buffer to store incoming events 
*  Type of data tobe stored could be configured from cbuffer.h
*  curently its set to uint8_t (0-100)
*  Buffer size is defined in main.h as EVENT_QUEUE_SIZE
*/
cbuffer_s * cbuffer = NULL;
/* Blink LED with required duty cycle
*  to store duty cycle, period , trigger and kill flag 
*  defined in main.h
*/
blinky_t led;
void led_turn_on();
void led_turn_off();

typedef struct
{
	uint32_t buffer[1000];
}arr_view_t;

arr_view_t * arr_view = NULL;

uint8_t * usb_t_buffer_p = NULL;
uint8_t usb_t_buffer[USB_T_TRANSMIT_SIZE];
timestamp_t time;
usb_packet_t * usb_t_packet = NULL;


uint8_t * usb_r_buffer_p = NULL;
uint8_t usb_r_buffer[USB_T_TRANSMIT_SIZE];
usb_packet_t * usb_r_packet = NULL;
bool usb_new_data_avail = FALSE;

int main(void)
{
	/* Local Variables */
	status_e status = STATUS_OK;

	/* Validation */

	/* Init Code */
	if(STATUS_OK == status)
	{
		status = low_level_init();
	}
	if(STATUS_OK == status)
	{
		status = systick_init(&systick_callback);
	}
	if(STATUS_OK == status)
	{
		 status = clock_init();
	}
	if(STATUS_OK == status)
	{
		status = systick_init(&systick_callback);
	}	
	if(STATUS_OK == status)
	{
		status = usb_init();
	}
	if(STATUS_OK == status)
	{
		status = cbuffer_init(&cbuffer, EVENT_QUEUE_SIZE);
	}
	if(STATUS_OK == status)
	{
		status = led_init();
	}
	if(STATUS_OK == status)
	{
		status = blinky_init(&led, 10000, 1000, 30, &led_turn_on, &led_turn_off);
	}

/*
 * cbuffer test routine
 * */
#ifdef _CBUFFER_TEST_
	cbuffer_write(cbuffer, 12);
	cbuffer_write(cbuffer, 13);
	cbuffer_write(cbuffer, 14);
//	cbuffer_write(&cbuffer, 15);
//	cbuffer_write(&cbuffer, 17);
//	cbuffer_write(&cbuffer, 18);
//	cbuffer_write(&cbuffer, 19);
#endif
	/* Run eternally */
	while(1)
	{
		if(STATUS_OK == status)
		{
			while(!usb_new_data_avail)
			{
				
			}
		}
		else
			break;
	}

	/* Return */
	return(status);
}

void systick_callback(void)
{
	// usb_send(NYMBLE);
	// do some shit
	blinky(&led);
}

status_e usb_init()
{
	status_e status = STATUS_OK;
	
	__HAL_RCC_GPIOA_CLK_ENABLE();

	MX_USB_DEVICE_Init();

	return (status);
}

char packet_0[] = "Nymble";
char packet_1[] = "Labs";
char packet_2[] = "NymbleLabs";

uint8_t dummy_test_data[20]=
{
		1,2,3,4,5,6,7,8,9,10,
		1,2,3,4,5,6,7,8,9,10
};

status_e usb_send(usb_packet_e type, uint32_t dc)
{
	status_e status = STATUS_OK;
	usb_t_packet = (usb_packet_t *)usb_t_buffer;

	usb_t_packet->usb_packet_u.usb_packet_s.headerid_u.headerid_s.header = 0x7E;

	switch(type)
	{
		case NYMBLE :
			usb_t_packet->usb_packet_u.usb_packet_s.headerid_u.headerid_s.id = 0x01;
			memcpy(usb_t_packet->usb_packet_u.usb_packet_s.data, packet_0, strlen(packet_0) );
			memset(usb_t_packet->usb_packet_u.usb_packet_s.data + strlen(packet_0), 0x0, USB_T_TRANSMIT_SIZE  -8U -  strlen(packet_0));

		break;

		case LABS :
			usb_t_packet->usb_packet_u.usb_packet_s.headerid_u.headerid_s.id = 0x02;
			memcpy(usb_t_packet->usb_packet_u.usb_packet_s.data, packet_1, strlen(packet_1) );
			memset(usb_t_packet->usb_packet_u.usb_packet_s.data + strlen(packet_1), 0x0, USB_T_TRANSMIT_SIZE  - 8U -  strlen(packet_1));
		break;

		case NYMBLE_LABS :
			usb_t_packet->usb_packet_u.usb_packet_s.headerid_u.headerid_s.id = 0x03;
			memcpy(usb_t_packet->usb_packet_u.usb_packet_s.data, packet_2, strlen(packet_2) );
			memset(usb_t_packet->usb_packet_u.usb_packet_s.data + strlen(packet_2), 0x0, USB_T_TRANSMIT_SIZE -  8U -  strlen(packet_2));
		break;

		case DUTY_CYCLE :
			usb_t_packet->usb_packet_u.usb_packet_s.headerid_u.headerid_s.id = 0x04;
			usb_t_packet->usb_packet_u.usb_packet_s.duty_cycle = dc;
			memset(usb_t_packet->usb_packet_u.usb_packet_s.data, 0x0, USB_T_TRANSMIT_SIZE - 8U);
		break;
	}

//	time.timestamp_u.timestamp = return_msecs();
//	usb_t_buffer[0] = time.timestamp_u.timestamp_s.timestamp_ar[0];
//	usb_t_buffer[1] = time.timestamp_u.timestamp_s.timestamp_ar[1];
//	usb_t_buffer[2] = time.timestamp_u.timestamp_s.timestamp_ar[2];
//	usb_t_buffer[3] = time.timestamp_u.timestamp_s.timestamp_ar[3];
//	usb_t_buffer[4] = 0xFF;
//	usb_t_buffer[5] = 0xFF;

	usb_t_buffer_p = usb_t_buffer;
	// USB_T_TRANSMIT_SIZE defined in evelynconfig.h
	status = USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS, usb_t_buffer_p , USB_T_TRANSMIT_SIZE);
	if(0 != status)
		exit(1);
	else
	  return (status);
}


void usb_receive()
{
	bool div4, div7;
	USBD_CUSTOM_HID_HandleTypeDef * husb = 	(USBD_CUSTOM_HID_HandleTypeDef *) hUsbDeviceFS.pClassData;

	memcpy(usb_r_buffer, husb->Report_buf, USB_T_TRANSMIT_SIZE);

	usb_r_packet = 	(usb_packet_t *) usb_r_buffer;
	usb_r_buffer_p = usb_r_buffer;

	if(0x7E == usb_r_packet->usb_packet_u.usb_packet_s.headerid_u.headerid_s.header)
	{
		div4 = 0 == usb_r_packet->usb_packet_u.usb_packet_s.duty_cycle % 4;
		div7 = 0 == usb_r_packet->usb_packet_u.usb_packet_s.duty_cycle % 7;
		if (1U == usb_r_packet->usb_packet_u.usb_packet_s.headerid_u.headerid_s.id)
		{
			if( div4 & !div7 )
			{
				usb_send(NYMBLE, usb_r_packet->usb_packet_u.usb_packet_s.duty_cycle);
			}
			else if(!div4 & div7)
			{
				usb_send(LABS, usb_r_packet->usb_packet_u.usb_packet_s.duty_cycle);
			}
			else if(div4 & div7)
			{
				usb_send(NYMBLE_LABS, usb_r_packet->usb_packet_u.usb_packet_s.duty_cycle);
			}
			else
			{

			}
			if(!div4 & !div7)
			{
				usb_send(DUTY_CYCLE, usb_r_packet->usb_packet_u.usb_packet_s.duty_cycle);
			}
			else
			{
				cbuffer_write(cbuffer, usb_r_packet->usb_packet_u.usb_packet_s.duty_cycle);
			}
		}

	}

	usb_new_data_avail = TRUE;

	return;
}

// USB init requires Hal Delay which has weak definition ins STM32 HAL libraries
uint32_t HAL_GetTick(void)
{
  return (return_msecs());
}


status_e led_init()
{
	/* Local Var */
	status_e status = STATUS_OK;

	/* Validation */

	/* code */
	if(STATUS_OK == status)
	{
		if(LED_PORT == GPIOD)
		{
			// enable clock to GPIOD
			RCC->AHB1ENR = (1U << 3U);

			// set corresponding mode bits to reset
			GPIOD->MODER &= ~(0b11 << (LED_PIN * 2U));
			// set corresponding mode bits to required mode
			GPIOD->MODER |=  (0b01 << (LED_PIN * 2U));
		}	
		else
		{
			status = STATUS_NOT_OK;
		}
	}	

	return (status);
}

void led_turn_on()
{
	GPIOD->BSRR |= (1U << LED_PIN);
}

void led_turn_off()
{
	GPIOD->BSRR |= (1U <<  (LED_PIN + 16U));
}

status_e blinky_init(blinky_t * blinky_p, uint32_t tperiod, uint32_t period, uint32_t duty_cycle, blinky_fp_t turn_on, blinky_fp_t turn_off)
{
	status_e status = STATUS_OK;

	if(NULL == blinky_p)
	{
		status = STATUS_NULL_PTR_EXCEPTION;
		return (status);
	}
	if(NULL == turn_on || NULL == turn_off)
	{
		status = STATUS_NULL_FPTR_EXCEPTION;
		return (status);		
	}
	if(duty_cycle > 100)
	{
		status = STATUS_INVALID_ARGUMENT;
		return (status);				
	}

	blinky_p->flag_u.flag_s.trigger   = 0x00U;
	blinky_p->flag_u.flag_s.kill      = 0x00U;
	blinky_p->flag_u.flag_s.led_state = 0x00U;

	// 10secs
	blinky_p->tperiod 	 = tperiod;
	// *Assumed 1sec
	blinky_p->period  	 = period;
	// default duty cycle 300/1000 = 30% runs for 10secs
	// led blinks for 300ms then staus off for 700ms
	// repeats for 10s
	blinky_p->duty_cycle = duty_cycle;

	blinky_p->start_time = 0U;
	blinky_p->led_on_time = 0U;

	blinky_p->turn_on = turn_on;
	blinky_p->turn_off = turn_off;

	blinky_p->flag_u.flag_s.init_done = 0xFFU;

	return (status);
}
/* functions to update LED duty cycle,trigger, period or kill flag */
status_e led_update_dc(blinky_t * blinky_p, uint32_t duty_cycle)
{
	status_e status = STATUS_OK;

	if(NULL == blinky_p)
	{
		status = STATUS_NULL_PTR_EXCEPTION;
		return (status);
	}
	blinky_p->duty_cycle = duty_cycle;
	// enable blinky with new duty cycle
	blinky_p->flag_u.flag_s.kill = 0x00U;

	return (status);	
}

status_e led_updatef_trg(blinky_t *blinky_p, uint8_t trigger)
{
	status_e status = STATUS_OK;

	if(NULL == blinky_p)
	{
		status = STATUS_NULL_PTR_EXCEPTION;
		return (status);
	}
	blinky_p->flag_u.flag_s.trigger = trigger;

	return (status);	
}
status_e led_updatef_pr(blinky_t *blinky_p, uint8_t kill)
{
	status_e status = STATUS_OK;

	if(NULL == blinky_p)
	{
		status = STATUS_NULL_PTR_EXCEPTION;
		return (status);
	}
	blinky_p->flag_u.flag_s.kill = kill;

	return (status);		
}

//#define LED_DUTY_CYCLE_CHECK_ROUTINE

uint8_t dc = 0U;
/*
	Total time, t = 10s
	Time period of pwm = 1sec(assumed)
	duty cycle = (0-100)
	duty cycle default = (300ms/1000ms ) * 100 = 30%
*/
void blinky(blinky_t * blinky_p)
{
	if(0xFF == blinky_p->flag_u.flag_s.init_done)
	{
		// if KILL is set TURN OF LED
		if(0xFF == blinky_p->flag_u.flag_s.kill)
		{
			// turn off the led
			if (NULL != blinky_p->turn_off)
			{
				(*blinky_p->turn_off) ();
				// set led state flag to 0x00 , OFF
				blinky_p->flag_u.flag_s.led_state = 0x00U;
				blinky_p->led_on_time = 0U;
				blinky_p->start_time = 0U;
				if(!is_cbuffer_empty(cbuffer))
				{
					cbuffer_read(cbuffer, &dc);
					led_update_dc(&led, dc);
				}

#ifdef LED_DUTY_CYCLE_CHECK_ROUTINE
#warning "Brian: update duty cycle here or in the main whe kill == 0xFF"
				dc += 5;
				if(dc == 100)
					dc = 0U;
				led_update_dc(&led, dc);
#endif /* LED_DUTY_CYCLE_CHECK_ROUTINE */
				return;
			}
			else
			{
				return;
			}
		}

		// led is turned ON
		if(0xFF == blinky_p->flag_u.flag_s.led_state)
		{
			// led turned OFF, 1 PWM TURN ON cycle complete 
			if( return_msecs() - blinky_p->led_on_time >= (blinky_p->duty_cycle * blinky_p->period)/100U )
			{
				// turn off the led
				if (NULL != blinky_p->turn_off)
				{
					(*blinky_p->turn_off) ();
					// set led state flag to 0x00, OFF
					blinky_p->flag_u.flag_s.led_state = 0x00U;		
				}
				else
				{
					return;
				}
			}			
		}
		// led is turned OFF
		else
		{
			if(0U == blinky_p->led_on_time && 0U == blinky_p->start_time)
			{
				// turn ON the led
				if (NULL != blinky_p->turn_on)
				{
					(*blinky_p->turn_on) ();
					// set led state flag to 0xFF
					blinky_p->flag_u.flag_s.led_state = 0xFFU;
				}
				else
				{
					return;
				}

				blinky_p->led_on_time = return_msecs();
				blinky_p->start_time = blinky_p->led_on_time;
			}
			// 10 sec total period end.
			else if(return_msecs() - blinky_p->start_time > blinky_p->tperiod)
			{
				// load new duty cycle
				blinky_p->flag_u.flag_s.kill = 0xFFU;
			}
			// turn ON LED 1 PWM Cycle end.
			else if(return_msecs() > blinky_p->led_on_time + blinky_p->period)
			{
				// turn ON the led
				if (NULL != blinky_p->turn_on)
				{
					(*blinky_p->turn_on) ();
					// set led state flag to 0xFF
					blinky_p->flag_u.flag_s.led_state = 0xFFU;
				}
				else
				{
					return;
				}

				blinky_p->led_on_time = return_msecs();
			}			
		}
	}
	return;
}

// err handler USB
void _Error_Handler(char * file, int line)
{

}

