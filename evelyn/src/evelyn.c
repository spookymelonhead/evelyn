/*
 * evelyn, STM32 bare-minimal. 
 * Copyright (c) 2017 - 2025
 * All Rights Reserved.
 * 
 * Hardik Madaan
 *
 * cn:      Brian doofus
 * email:   hardik.mad@gmail.com
 * web:     www.tachymoron.wordpress.com
 * git:     www.github.com/pundoobvi
 * bitbuck: https://bitbucket.org/pundoobvi/
 */
/**
  ******************************************************************************
  * @file           : evelyn.c
  * @brief          : holds systick, clock init, hw init function declarations 
  *                   isr handlers
  * @project				: evelyn
  * @purpose				: just for fun
  ******************************************************************************
 **/

/* Include files*/
#include "common_types.h"
#include "stddef.h"
#include "evelyn.h"
#include "evelynconfig.h"
#include "system_stm32f4xx.h"
#include "stm32f4xx_hal_pcd.h"
/* Macros */

/* Global Variables */
/* Systick globals */
systick_msecs_t msecs;
systick_callback_fp_t systick_callback_fp = NULL;

/* Function Definitions */
/**
  ******************************************************************************
  * @funtion        : return msecs
  * @brief          : returns milliseconds
  * @arguments      : brian doofus
  * @project        : evelyn
  * @comments       : 
  * @date           : 01/05/2018(mm-dd-yy)
  ******************************************************************************
 * */
systick_msecs_t return_msecs()
{
  return (msecs);
}

/**
  ******************************************************************************
  * @funtion        : delay
  * @brief          : milliseconds delay
  * @arguments      : brian doofus
  * @project        : evelyn
  * @comments       : 
  * @date           : 01/05/2018(mm-dd-yy)
  ******************************************************************************
 * */
void delay(uint32_t delay)
{
  systick_msecs_t start_msecs = return_msecs();

  while(return_msecs() - start_msecs < delay)
  {
    // do no thing
  }
}

/**
  ******************************************************************************
  * @funtion        : gpio init
  * @brief          : initializes gpio, alternate function, PuPd, Open drain etc.
  * @arguments      : brian doofus
  * @project        : evelyn
  * @comments       : 
  * @date           : 01/05/2018(mm-dd-yy)
  ******************************************************************************
 * */
status_e gpio_init()
{
  /* Local Variables */
  status_e status = STATUS_OK;

  /* Validation */

  /* Code */

  /* Return */
  return (status);
}

/**
  ******************************************************************************
  * @funtion        : clock init
  * @brief          : selects clock source for PLL, configures PLL, initializes
                      system core clock 
  * @arguments      : brian doofus
  * @project        : evelyn
  * @comments       : 
  * @date           : 01/05/2018(mm-dd-yy)
  ******************************************************************************
 * */
status_e clock_init()
{
  /* Local Variables */
  status_e status = STATUS_OK;
  uint32_t start_msecs = 0U;
  uint32_t tempreg = 0U;
  uint32_t p = 0U, m = 0U, n = 0U, q = 0U;
  /* Validation */

  /* Code */
  /*
    if HSE_ON and HSE_ON bit in RCC->CR
    check HSERDY flag

    if HSI HSICAL bits in RCC->CR
    check HSIRDY
  */  
#ifdef CPU_CLOCK_SOURCE_HSI
  // Enabale HSI
  RCC->CR |= (1U << 0U);
  start_msecs = return_msecs();
  // wait till HSI RDY is SET
  while( FALSE == (RCC->CR) >> 1U & 0x1)
  {
    if(return_msecs() - start_msecs > CLOCK_SOURCE_RDY_TIMEOUT)
    {
      status = STATUS_TIMEOUT;
      return (status);
    }
  }
#endif /* CPU_CLOCK_SOURCE_HSI */
#ifdef CPU_CLOCK_SOURCE_HSE

  #ifdef HSE_BYPASS
    // HSE Bypass
    RCC->CR |= (1U << 18U);
  #endif /* HSE_BYPASS */
  // Enabale HSE
  RCC->CR |= (1U << 16U);

  start_msecs = return_msecs();
  // wait till HSE RDY is SET
  while( FALSE == ((RCC->CR >> 17U) & 0x1) )
  {
    if(return_msecs() - start_msecs > CLOCK_SOURCE_RDY_TIMEOUT)
    {
      status = STATUS_TIMEOUT;
      return (status);
    }
  }

  #ifndef CPU_CLOCK_SOURCE_PLL_HSE
    RCC->CFGR &= ~((uint32_t)0x11U);
    RCC->CFGR |= 0b01;
  #endif /* CPU_CLOCK_SOURCE_PLL_HSE */
#endif /* CPU_CLOCK_SOURCE_HSE */

  /* Select regulator voltage output Scale 1 mode */
  RCC->APB1ENR |= (1U << 28U);
  PWR->CR &= ~PWR_CR_VOS;
  PWR->CR |= PWR_CR_VOS;

  // update flash latency
  FLASH->ACR |= 0x05U;

#if defined(CPU_CLOCK_SOURCE_PLL_HSI ) || defined (CPU_CLOCK_SOURCE_PLL_HSE) 

  // disable PLL
  RCC->CR &= ~(1U << 24U);
  start_msecs = return_msecs();
  // wait until bit is set to zero
  while( TRUE == ((RCC->CR >> 25U) & 0x1U))
  {
    if(return_msecs() - start_msecs > CLOCK_SOURCE_RDY_TIMEOUT)
    {
      status = STATUS_TIMEOUT;
      return (status);
    }
  }

  // select PLL source as HSE
  tempreg = 0U;
  
#ifdef CPU_CLOCK_SOURCE_PLL_HSI
  n = CPU_CLOCK_SPEED_MHZ;
  m = 8U;
  p = 2U;
  q = 7U;
  // set HSI as source
  tempreg &=  ~(1U << 22U);
#endif /* CPU_CLOCK_SOURCE_PLL_HSI */

#ifdef CPU_CLOCK_SOURCE_PLL_HSE
  n = CPU_CLOCK_SPEED_MHZ;
  m = 4U;
  p = 2U;
  q = 7U;
  // set HSE as source
  tempreg |=  (1U << 22U);
#endif /* CPU_CLOCK_SOURCE_PLL_HSE */  

  // set m
  tempreg |=  (m << 0U);
  //set n
  tempreg |=  (n << 6U);
  //set p
  tempreg |=  (((p-1) >>1) << 16U);
  // set q
  tempreg |=  (q << 24U);

  // write reg value
  RCC->PLLCFGR = tempreg;

  // enable PLL
  RCC->CR |= (1U << 24U);

  start_msecs = return_msecs();
  // wait until bit is set to zero
  while(FALSE ==  ((RCC->CR >> 25U) & 0x1U))
  {
    if(return_msecs() - start_msecs > CLOCK_SOURCE_RDY_TIMEOUT)
    {
      status = STATUS_TIMEOUT;
      return (status);
    }
  }

  tempreg = 0U;

  tempreg |= (5U << 10U);
  tempreg |= (4U << 13U);
  tempreg |= (0U << 4U);

  RCC->CFGR = tempreg;

  // wait till PLL ready
  if(TRUE != ((RCC->CR >> 25U) & 0x1U))
  {
    status = STATUS_NOT_OK;
    return (status);
  }

  RCC->CFGR &= ~((uint32_t) 0b11U);
  // switch clock source to PLL
  RCC->CFGR |= 0b10;

  start_msecs = return_msecs();
  while(0b10 != ((RCC->CFGR >> 2U) & 0b11) )
  {
    if(return_msecs() - start_msecs > CLOCK_SOURCE_RDY_TIMEOUT)
    {
      status = STATUS_TIMEOUT;
      RCC->CFGR &= ~(0b00);
      return (status);      
    }
  }

#endif /* CPU_CLOCK_SOURCE_PLL_HSI || CPU_CLOCK_SOURCE_PLL_HSE */
  
 SystemCoreClockUpdate();

  /* Return */
  return (status);
}

/**
  ******************************************************************************
  * @funtion        : systick_init
  * @brief          : starts systick timer for time keeping
  * @arguments			: brian doofus
  * @project				: evelyn
  * @comments				: 
  * @date 					: 01/05/2018(mm-dd-yy)
  ******************************************************************************
 **/
status_e systick_init(systick_callback_fp_t callback)
{
	/* Local Variables */
	status_e status = STATUS_OK;
	/* 24bit Reload Value */
	uint32_t reload = SystemCoreClock/config_systick.period - 1U;
	uint32_t prioritygroup = 0U;

	/* Validation */
  if (reload > 0xFFFFFFU)
  {
  	/* Reload value impossible */
  	status = STATUS_INVALID_CONFIG;
    return (status);
  }
  if(NULL == callback)
  {
  	/* Function pointer points to NULL */
  	status = STATUS_NULL_FPTR_EXCEPTION;
    return (status);  	
  }
	
	/* Code */
  /* set reload value */
  SysTick->LOAD  = (uint32_t)(reload);
  /* set priority for systick interrupt */
  /* Set Priority */
  NVIC_SetPriority (SysTick_IRQn, (1U << __NVIC_PRIO_BITS) - 1U);
  /* Load start value equal to zero */
  SysTick->VAL   = 0U;

  SysTick->CTRL  = ( 1U << 2U) | /* Select System clock / 1 as the Systick clock */
                   ( 1U << 1U) | /* Sytick interrupt enable on counting to zero */
                   ( 1U << 0U);  /* Start Counting */
	
	systick_callback_fp = callback;

  // get priority grouping
  prioritygroup = NVIC_GetPriorityGrouping();
  NVIC_SetPriority(SysTick_IRQn, NVIC_EncodePriority(prioritygroup, CONFIG_SYSTICK_PREEMPT_PRIORITY, CONFIG_SYSTICK_SUB_PRIORITY));
	
  /* Return */
	return (status);
}

/**
  ******************************************************************************
  * @funtion        : low_level_init
  * @brief          : low level hw init
  * @arguments			: brian doofus
  * @project				: evelyn
  * @comments				: 
  * @date 					: 01/05/2018(mm-dd-yy)
  ******************************************************************************
 * */
status_e low_level_init()
{
	/* Local Variables */
	status_e status = STATUS_OK;
  uint32_t prioritygroup = 0U;
	/* Validation */

	/* Code */
  /* Instruction cache, data cache and prefetch buffer enable */
  FLASH->ACR |= (1U << 8U);
  FLASH->ACR |= (1U << 9U);
  FLASH->ACR |= (1U << 10U);

  /* System interrupt init*/
  NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_0);
  prioritygroup = NVIC_GetPriorityGrouping();
  
  NVIC_SetPriority(MemoryManagement_IRQn, NVIC_EncodePriority(prioritygroup, 0, 0));
  NVIC_SetPriority(BusFault_IRQn, NVIC_EncodePriority(prioritygroup, 0, 0));
  NVIC_SetPriority(UsageFault_IRQn, NVIC_EncodePriority(prioritygroup, 0, 0));
  NVIC_SetPriority(SVCall_IRQn, NVIC_EncodePriority(prioritygroup, 0, 0));
  NVIC_SetPriority(DebugMonitor_IRQn, NVIC_EncodePriority(prioritygroup, 0, 0));
  NVIC_SetPriority(PendSV_IRQn, NVIC_EncodePriority(prioritygroup, 0, 0));
  NVIC_SetPriority(SysTick_IRQn, NVIC_EncodePriority(prioritygroup, 0, 0));

	/* NVIC Priority group setting */
   NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

	/* Return */
	return (status);
}

/**
  ******************************************************************************
  * @funtion        : low level init
  * @brief          : low level hw init
  * @arguments			: brian doofus
  * @project				: evelyn
  * @comments				: 
  * @date 					: 01/05/2018(mm-dd-yy)
  ******************************************************************************
 * */
status_e temp()
{
	/* Local Variables */
	status_e status = STATUS_OK;

	/* Validation */

	/* Code */

	/* Return */
	return (status);
}
/* Callbacks */

/* ISRs */

/**
  ******************************************************************************
  * @funtion        : systick_handler
  * @brief          : increments milliseocnds counter and calls the callback
  * @arguments			: brian doofus
  * @project				: evelyn
  * @comments				: 
  * @date 					: 01/05/2018(mm-dd-yy)
  ******************************************************************************
 * */
void systick_handler()
{
	/* Incrememnt milliseconds */
	msecs++;

	/* Invoke systick callback */
	if(NULL != systick_callback_fp)
		(*systick_callback_fp)();
}

extern PCD_HandleTypeDef hpcd_USB_OTG_FS;
/**
  ******************************************************************************
  * @funtion        : OTG_FS_IRQHandler
  * @brief          : OTG_FS_IRQHandler
  * @arguments      : brian doofus
  * @project        : evelyn
  * @comments       : 
  * @date           : 01/05/2018(mm-dd-yy)
  ******************************************************************************
 * */
void OTG_FS_IRQHandler(void)
{
  /* USER CODE BEGIN OTG_FS_IRQn 0 */

  /* USER CODE END OTG_FS_IRQn 0 */
  HAL_PCD_IRQHandler(&hpcd_USB_OTG_FS);
  /* USER CODE BEGIN OTG_FS_IRQn 1 */

  /* USER CODE END OTG_FS_IRQn 1 */
}
