/*
 * evelyn, STM32 bare-minimal. 
 * Copyright (c) 2017 - 2025
 * All Rights Reserved.
 * 
 * Hardik Madaan
 *
 * cn:      Brian doofus
 * email:   hardik.mad@gmail.com
 * web:     www.tachymoron.wordpress.com
 * git:     www.github.com/pundoobvi
 * bitbuck: https://bitbucket.org/pundoobvi/
 */
/**
  ******************************************************************************
  * @file           : evelyn.c
  * @brief          : holds config structure definitions
  * @author					: brian doofus
  * @project				: evelyn
  * @purpose				: just for fun
  ******************************************************************************
 **/

/* Include files*/
#include "evelyn.h"
#include "evelynconfig.h"

/* Global Variables */

config_systick_t config_systick = {
/* (system_core_clock/tick) * (1/system_core_clock) = 1ms */
1000U,
};

/* Function Definitions */
