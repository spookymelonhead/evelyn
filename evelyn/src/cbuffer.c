/*
 * cBuffer, circular buffer implementation. 
 * Copyright (c) 2017 - 2025
 * All Rights Reserved.
 * 
 * Hardik Madaan
 *
 * cn:      Brian doofus
 * email:   hardik.mad@gmail.com
 * web:     www.tachymoron.wordpress.com
 * git:     www.github.com/pundoobvi
 * bitbuck: https://bitbucket.org/pundoobvi/
 */
/**
  ******************************************************************************
  * @file           : cbuffer.h
  * @brief          : holds circular buffer function definitions.
  * @project				: evelyn
  * @purpose				: just for fun
  ******************************************************************************
 **/
#include "cbuffer.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

bool is_cbuffer_empty(cbuffer_s * cbuffer)
{
	bool state = FALSE;

	if(NULL == cbuffer)
	{
		return (1);	
	}

	state = cbuffer->read == cbuffer->write;
	return (state);
}

bool is_cbuffer_full(cbuffer_s * cbuffer)
{
	bool state = FALSE;

	if(NULL == cbuffer)
	{
		return (1);	
	}

	state = cbuffer->read == ( (cbuffer->write + 1) % cbuffer->size);
	return (state);
}


bool cbuffer_init(cbuffer_s ** cbuffer_dp, uint32_t size)
{
	bool status = 0;

	if(NULL == (cbuffer_dp))	
	{
		status = 1;
		return (status);
	}

	if(0 == status)
	{
		*(cbuffer_dp) = (cbuffer_s *) malloc( sizeof(cbuffer_s) );
		// if memory allocation fails
		if(NULL == (*cbuffer_dp))
		{
			status = 1;
			return (status);		
		}	
		else
		{
			(*cbuffer_dp)->buffer = (cbuf_data_t *) malloc(size  * sizeof(cbuf_data_t) );
			// if memory allocation fails
			if(NULL == (*cbuffer_dp)->buffer)	
			{
				status = 1;
				return (status);
			}
			else
			{
				memset( (void *) (*cbuffer_dp)->buffer, 0x0, size  * sizeof(cbuf_data_t) );
			}
		}
	}
	
	if(0 == status)
	{
		status = cbuffer_set_to_default(*cbuffer_dp, size, FALSE);
  }

  return (status);
}

bool cbuffer_write(cbuffer_s *cbuffer , cbuf_data_t data)
{
	bool status = 0;
	
	if(NULL == cbuffer)
	{
		return (status);	
	}

	cbuffer->buffer[cbuffer->write] = data;
	cbuffer->write = (uint32_t)( (cbuffer->write + 1U) % cbuffer->size);
	if(cbuffer->write == cbuffer->read)
	{
		cbuffer->read = (cbuffer->read + 1U) % cbuffer->size;
	}
	return (status);
}

bool cbuffer_read(cbuffer_s *cbuffer , cbuf_data_t * data_p)
{
	bool status = 0;

	if(NULL == cbuffer)
	{
		return (status);	
	}
	if(TRUE == is_cbuffer_empty(cbuffer) )
	{
		return 0;
	}

	if(NULL ==  data_p)
	{
		status = 1;
		return (status);
	}

	if(0 == status)
	{
		*data_p = cbuffer->buffer[cbuffer->read];
		cbuffer->read = (cbuffer->read + 1U) % cbuffer->size;
	}
	return (status);
}

bool cbuffer_set_to_default(cbuffer_s * cbuffer, uint32_t size, uint8_t resetf)
{
	bool status = 0;
	if(NULL == cbuffer)
	{
		status = 1;		
		return (status);	
	}

	if(TRUE == resetf)
	{
		free(cbuffer->buffer);
	}
	else
	{
		// do nothing
	}

	cbuffer->size = size; 
	cbuffer->write = 0;
	cbuffer->read = 0;
	cbuffer->full = 0;

	return (status);
}

	#ifdef _PRINTF_
		void cbuffer_print(cbuffer_s *cbuffer)
		{
			uint32_t counter = 0U;
			if(NULL == cbuffer)
				return;

				printf("\ncBuffer:");
				printf("\n\tSize @ %u Read @ %u Write @ %u \t", cbuffer->size, cbuffer->read, cbuffer->write);
			if(cbuffer->full)
				printf("Full");
			else
				printf("NotFull");

			printf("\ncBuffer's content\n\t");
			for(counter = 0;  counter < cbuffer->size ; counter++)
			{
				printf("%u \t", cbuffer->buffer[counter]);
			}
		}
	#endif /* _PRINTF_ */
