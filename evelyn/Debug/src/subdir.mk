################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/cbuffer.c \
../src/evelyn.c \
../src/evelynconfig.c \
../src/main.c \
../src/stm32f4xx_it.c \
../src/syscalls.c \
../src/system_stm32f4xx.c \
../src/usb_device.c \
../src/usbd_conf.c \
../src/usbd_core.c \
../src/usbd_ctlreq.c \
../src/usbd_custom_hid_if.c \
../src/usbd_customhid.c \
../src/usbd_desc.c \
../src/usbd_ioreq.c 

OBJS += \
./src/cbuffer.o \
./src/evelyn.o \
./src/evelynconfig.o \
./src/main.o \
./src/stm32f4xx_it.o \
./src/syscalls.o \
./src/system_stm32f4xx.o \
./src/usb_device.o \
./src/usbd_conf.o \
./src/usbd_core.o \
./src/usbd_ctlreq.o \
./src/usbd_custom_hid_if.o \
./src/usbd_customhid.o \
./src/usbd_desc.o \
./src/usbd_ioreq.o 

C_DEPS += \
./src/cbuffer.d \
./src/evelyn.d \
./src/evelynconfig.d \
./src/main.d \
./src/stm32f4xx_it.d \
./src/syscalls.d \
./src/system_stm32f4xx.d \
./src/usb_device.d \
./src/usbd_conf.d \
./src/usbd_core.d \
./src/usbd_ctlreq.d \
./src/usbd_custom_hid_if.d \
./src/usbd_customhid.d \
./src/usbd_desc.d \
./src/usbd_ioreq.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DSTM32 -DSTM32F4 -DSTM32F407VGTx -DSTM32F4DISCOVERY -DDEBUG -DSTM32F407xx -DUSE_HAL_DRIVER -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ili9325" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/s25fl512s" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/cs43l22" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ili9341" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ampire480272" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/n25q512a" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/s5k5cag" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/mfxstm32l152" -I"/home/brian/Desktop/code/evelyn/CMSIS/device" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/n25q128a" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ts3510" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/st7735" -I"/home/brian/Desktop/code/evelyn/HAL_Driver/Inc/Legacy" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/lis302dl" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/otm8009a" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/stmpe1600" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/Common" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ov2640" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/l3gd20" -I"/home/brian/Desktop/code/evelyn/HAL_Driver/Inc" -I"/home/brian/Desktop/code/evelyn/Utilities" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/stmpe811" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/lis3dsh" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/wm8994" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/n25q256a" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ls016b8uy" -I"/home/brian/Desktop/code/evelyn/inc" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ft6x06" -I"/home/brian/Desktop/code/evelyn/Utilities/STM32F4-Discovery" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/exc7200" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/st7789h2" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ampire640480" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/lsm303dlhc" -I"/home/brian/Desktop/code/evelyn/CMSIS/core" -I"/home/brian/Desktop/code/evelyn/middleware/STM32_USB_Device_Library/Class/CustomHID/Inc" -I"/home/brian/Desktop/code/evelyn/middleware/STM32_USB_Device_Library/Core/Inc" -I"/home/brian/Desktop/code/evelyn/inc" -I"/home/brian/Desktop/code/evelyn/HAL_Driver/Inc" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


