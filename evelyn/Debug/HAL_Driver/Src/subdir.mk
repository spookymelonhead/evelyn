################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../HAL_Driver/Src/stm32f4xx_hal.c \
../HAL_Driver/Src/stm32f4xx_hal_cortex.c \
../HAL_Driver/Src/stm32f4xx_hal_dma.c \
../HAL_Driver/Src/stm32f4xx_hal_dma_ex.c \
../HAL_Driver/Src/stm32f4xx_hal_flash.c \
../HAL_Driver/Src/stm32f4xx_hal_flash_ex.c \
../HAL_Driver/Src/stm32f4xx_hal_flash_ramfunc.c \
../HAL_Driver/Src/stm32f4xx_hal_gpio.c \
../HAL_Driver/Src/stm32f4xx_hal_pcd.c \
../HAL_Driver/Src/stm32f4xx_hal_pcd_ex.c \
../HAL_Driver/Src/stm32f4xx_hal_pwr.c \
../HAL_Driver/Src/stm32f4xx_hal_pwr_ex.c \
../HAL_Driver/Src/stm32f4xx_hal_rcc.c \
../HAL_Driver/Src/stm32f4xx_hal_rcc_ex.c \
../HAL_Driver/Src/stm32f4xx_ll_dma.c \
../HAL_Driver/Src/stm32f4xx_ll_gpio.c \
../HAL_Driver/Src/stm32f4xx_ll_pwr.c \
../HAL_Driver/Src/stm32f4xx_ll_rcc.c \
../HAL_Driver/Src/stm32f4xx_ll_usb.c 

OBJS += \
./HAL_Driver/Src/stm32f4xx_hal.o \
./HAL_Driver/Src/stm32f4xx_hal_cortex.o \
./HAL_Driver/Src/stm32f4xx_hal_dma.o \
./HAL_Driver/Src/stm32f4xx_hal_dma_ex.o \
./HAL_Driver/Src/stm32f4xx_hal_flash.o \
./HAL_Driver/Src/stm32f4xx_hal_flash_ex.o \
./HAL_Driver/Src/stm32f4xx_hal_flash_ramfunc.o \
./HAL_Driver/Src/stm32f4xx_hal_gpio.o \
./HAL_Driver/Src/stm32f4xx_hal_pcd.o \
./HAL_Driver/Src/stm32f4xx_hal_pcd_ex.o \
./HAL_Driver/Src/stm32f4xx_hal_pwr.o \
./HAL_Driver/Src/stm32f4xx_hal_pwr_ex.o \
./HAL_Driver/Src/stm32f4xx_hal_rcc.o \
./HAL_Driver/Src/stm32f4xx_hal_rcc_ex.o \
./HAL_Driver/Src/stm32f4xx_ll_dma.o \
./HAL_Driver/Src/stm32f4xx_ll_gpio.o \
./HAL_Driver/Src/stm32f4xx_ll_pwr.o \
./HAL_Driver/Src/stm32f4xx_ll_rcc.o \
./HAL_Driver/Src/stm32f4xx_ll_usb.o 

C_DEPS += \
./HAL_Driver/Src/stm32f4xx_hal.d \
./HAL_Driver/Src/stm32f4xx_hal_cortex.d \
./HAL_Driver/Src/stm32f4xx_hal_dma.d \
./HAL_Driver/Src/stm32f4xx_hal_dma_ex.d \
./HAL_Driver/Src/stm32f4xx_hal_flash.d \
./HAL_Driver/Src/stm32f4xx_hal_flash_ex.d \
./HAL_Driver/Src/stm32f4xx_hal_flash_ramfunc.d \
./HAL_Driver/Src/stm32f4xx_hal_gpio.d \
./HAL_Driver/Src/stm32f4xx_hal_pcd.d \
./HAL_Driver/Src/stm32f4xx_hal_pcd_ex.d \
./HAL_Driver/Src/stm32f4xx_hal_pwr.d \
./HAL_Driver/Src/stm32f4xx_hal_pwr_ex.d \
./HAL_Driver/Src/stm32f4xx_hal_rcc.d \
./HAL_Driver/Src/stm32f4xx_hal_rcc_ex.d \
./HAL_Driver/Src/stm32f4xx_ll_dma.d \
./HAL_Driver/Src/stm32f4xx_ll_gpio.d \
./HAL_Driver/Src/stm32f4xx_ll_pwr.d \
./HAL_Driver/Src/stm32f4xx_ll_rcc.d \
./HAL_Driver/Src/stm32f4xx_ll_usb.d 


# Each subdirectory must supply rules for building sources it contributes
HAL_Driver/Src/%.o: ../HAL_Driver/Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DSTM32 -DSTM32F4 -DSTM32F407VGTx -DSTM32F4DISCOVERY -DDEBUG -DSTM32F407xx -DUSE_HAL_DRIVER -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ili9325" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/s25fl512s" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/cs43l22" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ili9341" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ampire480272" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/n25q512a" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/s5k5cag" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/mfxstm32l152" -I"/home/brian/Desktop/code/evelyn/CMSIS/device" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/n25q128a" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ts3510" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/st7735" -I"/home/brian/Desktop/code/evelyn/HAL_Driver/Inc/Legacy" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/lis302dl" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/otm8009a" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/stmpe1600" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/Common" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ov2640" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/l3gd20" -I"/home/brian/Desktop/code/evelyn/HAL_Driver/Inc" -I"/home/brian/Desktop/code/evelyn/Utilities" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/stmpe811" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/lis3dsh" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/wm8994" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/n25q256a" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ls016b8uy" -I"/home/brian/Desktop/code/evelyn/inc" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ft6x06" -I"/home/brian/Desktop/code/evelyn/Utilities/STM32F4-Discovery" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/exc7200" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/st7789h2" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ampire640480" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/lsm303dlhc" -I"/home/brian/Desktop/code/evelyn/CMSIS/core" -I"/home/brian/Desktop/code/evelyn/HAL_Driver/Inc" -I"/home/brian/Desktop/code/evelyn/inc" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


