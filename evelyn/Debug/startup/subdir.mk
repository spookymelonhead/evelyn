################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
../startup/startup_stm32f407xx.s 

OBJS += \
./startup/startup_stm32f407xx.o 


# Each subdirectory must supply rules for building sources it contributes
startup/%.o: ../startup/%.s
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Assembler'
	@echo $(PWD)
	arm-none-eabi-as -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ili9325" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/s25fl512s" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/cs43l22" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ili9341" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ampire480272" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/n25q512a" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/s5k5cag" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/mfxstm32l152" -I"/home/brian/Desktop/code/evelyn/CMSIS/device" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/n25q128a" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ts3510" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/st7735" -I"/home/brian/Desktop/code/evelyn/HAL_Driver/Inc/Legacy" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/lis302dl" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/otm8009a" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/stmpe1600" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/Common" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ov2640" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/l3gd20" -I"/home/brian/Desktop/code/evelyn/HAL_Driver/Inc" -I"/home/brian/Desktop/code/evelyn/Utilities" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/stmpe811" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/lis3dsh" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/wm8994" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/n25q256a" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ls016b8uy" -I"/home/brian/Desktop/code/evelyn/inc" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ft6x06" -I"/home/brian/Desktop/code/evelyn/Utilities/STM32F4-Discovery" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/exc7200" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/st7789h2" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/ampire640480" -I"/home/brian/Desktop/code/evelyn/Utilities/Components/lsm303dlhc" -I"/home/brian/Desktop/code/evelyn/CMSIS/core" -g -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


