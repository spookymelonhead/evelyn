/*
 * evelyn, STM32 bare-minimal. 
 * Copyright (c) 2017 - 2025
 * All Rights Reserved.
 * 
 * Hardik Madaan
 *
 * cn:      Brian doofus
 * email:   hardik.mad@gmail.com
 * web:     www.tachymoron.wordpress.com
 * git:     www.github.com/pundoobvi
 * bitbuck: https://bitbucket.org/pundoobvi/
 */
/**
  ******************************************************************************
  * @file           : common_types.h
  * @brief          : common salt
  * @author					: brian doofus
  * @project				: evelyn
  * @purpose				: just for fun
  ******************************************************************************
 * */
#ifndef _COMMON_TYPES_H_
#define _COMMON_TYPES_H_

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

#include "stdint.h"
#include "stdbool.h"
#include "stddef.h"

#ifndef NULL
	#define NULL (void *) (0U)
#endif /* NULL */
 	
#ifndef TRUE
 	#define TRUE (1U)
#endif /* TRUE */

#ifndef FALSE
 	#define FALSE (0U)
#endif /* FALSE */

#ifndef OK
 	#define OK (0U)
#endif /* OK */

#ifndef NOT_OK
 	#define NOT_OK !(OK)
#endif /* NOT_OK */

//#ifndef DISABLE
//  #define DISABLE (0U)
//#endif /* DISABLE */
//
//#ifndef ENABLE
//  #define ENABLE (1U)
//#endif /* ENABLE */

typedef enum
{
	STATUS_OK												= OK,
	STATUS_NOT_OK										= NOT_OK,

	STATUS_NULL_PTR_EXCEPTION,
	STATUS_NULL_FPTR_EXCEPTION,
	STATUS_TIMEOUT,
	STATUS_MEM_ALLOC_FAIL,
	STATUS_INVALID_SEQUENCE,
	STATUS_INVALID_CONFIG,
	STATUS_INVALID_ARGUMENT,

	STATUS_INVALID,
	STATUS_NUM											= STATUS_INVALID
	
}status_e;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _COMMON_TYPES_H_ */
