/*
 * evelyn, STM32 bare-minimal. 
 * Copyright (c) 2017 - 2025
 * All Rights Reserved.
 * 
 * Hardik Madaan
 *
 * cn:      Brian doofus
 * email:   hardik.mad@gmail.com
 * web:     www.tachymoron.wordpress.com
 * git:     www.github.com/pundoobvi
 * bitbuck: https://bitbucket.org/pundoobvi/
 */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : holds common shared file inclusions, typedefinitions
  * @author					: brian doofus
  * @project				: evelyn
  * @purpose				: just for fun
  ******************************************************************************
 * */
#ifndef _MAIN_H_
#define _MAIN_H_

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

#include "evelyn.h"
#include "evelynconfig.h"

/* Workround for not using HAL */
 /**
   * @brief  HAL Status structures definition
   */
 typedef enum
 {
   HAL_OK       = 0x00U,
   HAL_ERROR    = 0x01U,
   HAL_BUSY     = 0x02U,
   HAL_TIMEOUT  = 0x03U
 } HAL_StatusTypeDef;

 /**
   * @brief  HAL Lock structures definition
   */
 typedef enum
 {
   HAL_UNLOCKED = 0x00U,
   HAL_LOCKED   = 0x01U
 } HAL_LockTypeDef;


/* USB packet typedef */
typedef struct
{
	union
	{
		uint8_t packet[USB_T_TRANSMIT_SIZE];

		struct
		{
			union
			{
				uint32_t headerlen;
			
				struct
				{
					uint16_t header;
					uint16_t id;
			
				}headerid_s;
			
			}headerid_u;
			
			uint32_t duty_cycle;
			uint8_t data[USB_T_TRANSMIT_SIZE - 8U];

		}usb_packet_s;

	}usb_packet_u;

}usb_packet_t;

typedef enum
{
	NYMBLE					= 0U,
	LABS						= 1U,
	NYMBLE_LABS 		= 2U,
	DUTY_CYCLE 			= 3U,
	
}usb_packet_e;

/* Application config */
#define EVENT_QUEUE_SIZE (1000U)
#define LED_PIN 			(12U)
#define LED_PORT 			(GPIOD)

typedef enum
{
	EVELYN_SM_DANGLING					= 0U,
	EVELYN_SM_LOW_LEVEL_INIT,
	EVELYN_SM_SYSTICK_INIT,
	EVELYN_SM_CLOCK_INIT,
	EVELYN_SM_GPIO_INIT,
	EVELYN_SM_APPLICATION_INIT,

	EVELYN_SM_INVALID,
	EVELYN_SM_NUM 							= EVELYN_SM_INVALID
	
}evelyn_sm_e;

typedef struct
{
	union
	{
		uint32_t timestamp;
		struct
		{
			uint8_t timestamp_ar[4U];
		}timestamp_s;;

	}timestamp_u;

}timestamp_t;

/* Function pointer typedefinition for functions turning ON and turning OFF LEDS */
typedef void (*blinky_fp_t)(void);

/* Typedefinition to store led blinking data */
typedef struct
{
	union
	{
		/* Flag */
		uint32_t flag;
		struct
		{
			/* set to 0xFF to trigger */
			uint8_t trigger;
			/* KILL flag, Led wont blink even if triggered
			* Set to 0xFF to kill.
			*/
			uint8_t kill;
			/* LED state ON or OFF, 
			0x00 for OFF 
			0xFF for ON */
			uint8_t led_state;
			/* init_done 
			set to 0xFF if init done */
			uint8_t init_done;			
		}flag_s;
	}flag_u;
	/* Total period For How long LED needs to Blink 10000 milliseocnds */
	uint32_t tperiod;
	/* T period of the PWM */
	uint32_t period;
	/**/
	/* Duty cycle at which LED needs to blink 0-100*/
	uint32_t duty_cycle;

	/* 10 second cycle start time */
	systick_msecs_t start_time;
	/* time at which led was turned on for one PWM cycle */
	systick_msecs_t led_on_time;	
	/* Function pointer to hold 
	*  Function that turns on the led 
	*/
	blinky_fp_t turn_on;
	/* Function pointer to hold 
	*  Function that turns off the led 
	*/
	blinky_fp_t turn_off;
}blinky_t;

status_e blinky_init(blinky_t * blinky_p, uint32_t tperiod, uint32_t period, uint32_t duty_cycle, blinky_fp_t turn_on, blinky_fp_t turn_off);
/* functions to update LED duty cycle,trigger, period or kill flag */
status_e led_update_dc(blinky_t * blinky_p, uint32_t duty_cycle);

status_e led_updatef_trg(blinky_t *blinky_p, uint8_t trigger);
status_e led_updatef_pr(blinky_t *blinky_p, uint8_t kill);

/* Blinky to be put into systick callack */
void blinky();
/* Systick callback */
void systick_callback(void);
/* LED Init */
status_e led_init();
/* USB Init */
status_e usb_init();
status_e usb_send(usb_packet_e type, uint32_t dc);
void usb_receive();
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _MAIN_H_ */
