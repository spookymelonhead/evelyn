/*
 * cBuffer, circular buffer implementation. 
 * Copyright (c) 2017 - 2025
 * All Rights Reserved.
 * 
 * Hardik Madaan
 *
 * cn:      Brian doofus
 * email:   hardik.mad@gmail.com
 * web:     www.tachymoron.wordpress.com
 * git:     www.github.com/pundoobvi
 * bitbuck: https://bitbucket.org/pundoobvi/
 */
/**
  ******************************************************************************
  * @file           : cbuffer.h
  * @brief          : holds circular buffer typedefinitions, Macros, structures.
  * @project				: evelyn
  * @purpose				: just for fun
  ******************************************************************************
 **/
#ifndef _CBUFFER_H_
#define _CBUFFER_H_

#include "common_types.h"

#include "stdint.h"
#include "stdbool.h"

#define _DEBUG_EN_

/* typedefintion for datatype of data to be stored in the buffer */
typedef uint32_t cbuf_data_t;

/* typedef strcuture for circular buffer */
typedef struct
{
	cbuf_data_t * buffer;
	uint32_t size;

	uint32_t write, read;
	bool full;

}cbuffer_s;

/* is circular buffer empty */
bool is_cbuffer_empty(cbuffer_s * cbuffer);

/* is circular buffer full */
bool is_cbuffer_full(cbuffer_s * cbuffer);

/* inits circular buffer */
bool cbuffer_init(cbuffer_s ** cbuffer, uint32_t size);

/* write data into buffer */
bool cbuffer_write(cbuffer_s * cbuffer , cbuf_data_t data);

/* read data from buffer */
bool cbuffer_read(cbuffer_s * cbuffer, cbuf_data_t * data_p);

/* circular buffer set to default */
bool cbuffer_set_to_default(cbuffer_s * cbuffer, uint32_t size, uint8_t resetf);

/* write bulk data into buffer */
bool cbuffer_write_l(cbuffer_s * cbuffer, cbuf_data_t * data_p, uint32_t size);

	#ifdef _PRINTF_
		/* print cBuffer */
		void cbuffer_print(cbuffer_s *cbuffer);
	#endif /* _PRINTF_ */

#endif /* _CBUFFER_H_ */
