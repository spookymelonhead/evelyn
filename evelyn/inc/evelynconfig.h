/*
 * evelyn, STM32 bare-minimal. 
 * Copyright (c) 2017 - 2025
 * All Rights Reserved.
 * 
 * Hardik Madaan
 *
 * cn:      Brian doofus
 * email:   hardik.mad@gmail.com
 * web:     www.tachymoron.wordpress.com
 * git:     www.github.com/pundoobvi
 * bitbuck: https://bitbucket.org/pundoobvi/
 */
/**
  ******************************************************************************
  * @file           : evelynconfig.h
  * @brief          : holds config structures definitions
  * @author					: brian doofus
  * @project				: evelyn
  * @purpose				: just for fun
  ******************************************************************************
 * */
#ifndef _EVELYNCONFIG_H_
#define _EVELYNCONFIG_H_

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

/********************************************************************************
SYSTEM CONFIG
*********************************************************************************/
/* Clock Config Params */
/* enum defeinituion for clock sources */

// #define CPU_CLOCK_SOURCE_HSI

//#define CPU_CLOCK_SOURCE_HSE

// #define CPU_CLOCK_SOURCE_PLL_HSI

 #define CPU_CLOCK_SOURCE_PLL_HSE

#ifdef CPU_CLOCK_SOURCE_PLL_HSI
  #define CPU_CLOCK_SOURCE_HSI
#endif /* CPU_CLOCK_SOURCE_PLL_HSI */

#ifdef CPU_CLOCK_SOURCE_PLL_HSE
  #define CPU_CLOCK_SOURCE_HSE
#endif /* CPU_CLOCK_SOURCE_PLL_HSE */

// #define HSE_BYPASS

#define CPU_CLOCK_SPEED_MHZ (168U)

/* Systick Config Params */
#define CONFIG_SYSTICK_PREEMPT_PRIORITY (0U)
#define CONFIG_SYSTICK_SUB_PRIORITY 		(0U)

#define CLOCK_SOURCE_RDY_TIMEOUT (2U)

//#define USE_USB
#ifdef USE_USB
  #ifndef CPU_CLOCK_SOURCE_PLL_HSE
    #error "USB periph requires 48MHz clock, USE EXTERNAL CRYSTAL WITH PLL ON"
  #endif /* !CPU_CLOCK_SOURCE_PLL_HSE */
#endif /* USE_USB */  

typedef struct{
	/* (system_core_clock/tick) * (1/system_core_clock) = 1ms */
	uint32_t period;

}config_systick_t;

/* extern definition for systick config */
extern config_systick_t config_systick;

/* USB Config Params */
#define CONFIG_USB_HID_TRANSMIT_REPORT_COUNT (2U)
#define CONFIG_USB_HID_RECEIVE_REPORT_COUNT  (2U)

/* Timer Config Params */

/* USB Config Params */
#define USB_HID_POLLING_INTERVAL_IN_MS (0x01U)
#define USB_HID_MAX_PACKET_SIZE (64U)
// Should be the same for USB HID CLASS
#define USB_T_BUFFER_SIZE (USB_HID_MAX_PACKET_SIZE)

#define USB_T_TRANSMIT_SIZE (20U)
/********************************************************************************
APPLICATION CONFIG
*********************************************************************************/

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _EVELYN_H_ */
