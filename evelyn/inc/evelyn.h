/*
 * evelyn, STM32 bare-minimal. 
 * Copyright (c) 2017 - 2025
 * All Rights Reserved.
 * 
 * Hardik Madaan
 *
 * cn:      Brian doofus
 * email:   hardik.mad@gmail.com
 * web:     www.tachymoron.wordpress.com
 * git:     www.github.com/pundoobvi
 * bitbuck: https://bitbucket.org/pundoobvi/
 */
/**
  ******************************************************************************
  * @file           : evelyn.h
  * @brief          : holds, data structures, function protypes for hw drivers 
  *										and application
  * @author					: brian doofus
  * @project				: evelyn
  * @purpose				: just for fun
  ******************************************************************************
 * */
#ifndef _EVELYN_H_
#define _EVELYN_H_

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

#include "stm32f407xx.h"
#include "common_types.h"

/* NVIC Priority grouping */
#define NVIC_PRIORITYGROUP_4         0x00000003U 
#define NVIC_PRIORITYGROUP_0         0x00000007U 	

void delay(uint32_t delay);
status_e gpio_init();

/* timer stuff
pwm and stuff */
status_e timer_init();

/* System clock init */
status_e clock_init();

/* systick
for timing delay and stuff */
/* typedefinition for milliseconds */
typedef uint32_t systick_msecs_t;
typedef void (*systick_callback_fp_t)(void);
status_e systick_init();

void systick_handler();

void OTG_FS_IRQHandler();

systick_msecs_t return_msecs();

/* Low Level stuff init */
status_e low_level_init();

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _EVELYN_H_ */
