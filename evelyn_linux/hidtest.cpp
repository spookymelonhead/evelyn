#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "hidapi.h"
#include "stdint.h"
#include <sys/time.h>
#include "string.h"
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include "stddef.h"

#ifndef TRUE
 	#define TRUE (1U)
#endif /* TRUE */

#ifndef FALSE
 	#define FALSE (0U)
#endif /* FALSE */

#define VendorID (0x0483)

#define ProductID (0x5750)

#define SerialNo (0x00000000001A)

// #define _USB_HID_TIMESTAMP_

using namespace std;

#define USB_T_TRANSMIT_SIZE (20U)

#define HID_RECIEVE_DATA_SIZE (USB_T_TRANSMIT_SIZE)

#define HID_TRANSMIT_DATA_SIZE (USB_T_TRANSMIT_SIZE)

/* USB packet typedef */
typedef struct
{
	union
	{
		uint8_t packet[USB_T_TRANSMIT_SIZE];

		struct
		{
			union
			{
				uint32_t headerid;
			
				struct
				{
					uint16_t header;
					uint16_t len;
			
				}headerid_s;
			
			}headerid_u;
			
			uint32_t duty_cycle;

			uint8_t data[USB_T_TRANSMIT_SIZE - 8U];

		}usb_packet_s;

	}usb_packet_u;

}usb_packet_t;

#ifdef _USB_HID_TIMESTAMP_
typedef struct
{
	union
	{
		uint32_t timestamp;
		struct
		{
			uint8_t timestamp_ar[4U];
		}timestamp_s;;
	}timestamp_u;

}timestamp_t;


timestamp_t tame;

#endif 

usb_packet_t * usb_t_packet = NULL;
usb_packet_t * usb_r_packet = NULL;

uint8_t usb_t_buffer[USB_T_TRANSMIT_SIZE];
uint8_t usb_r_buffer[USB_T_TRANSMIT_SIZE];

uint8_t * usb_t_buffer_p = NULL;

long long current_timestamp();

// Read requested state
uint64_t last_tick, curr_tick, diff;


int res;
uint8_t buf[HID_RECIEVE_DATA_SIZE];

hid_device *handle;
int i;

// Enumerate and print the HID devices on the system
struct hid_device_info *devs, *cur_dev;
bool is_packet_expected = FALSE;

void print_ar(uint8_t * arr, uint32_t size)
{
	uint32_t counter = 0U;
	printf("\nPrinted array:\t");
	for (counter = 0U; counter< size; counter++)
	{
		printf("\t %u", arr[counter]);
	}
}

int main(int argc, char* argv[])
{
	
	devs = hid_enumerate(0x00, 0x00);
	cur_dev = devs;	
	while (cur_dev) 
	{
		printf("Device Found\n  type: %04hx %04hx\n  path: %s\n  serial_number: %ls",
		cur_dev->vendor_id, cur_dev->product_id, cur_dev->path, cur_dev->serial_number);
		printf("\n");
		printf("  Manufacturer: %ls\n", cur_dev->manufacturer_string);
		printf("  Product:      %ls\n", cur_dev->product_string);
		printf("\n");
		cur_dev = cur_dev->next;
	}
	hid_free_enumeration(devs);


	// Open the device using the VID, PID,
	// and optionally the Serial number.
	handle = hid_open(VendorID, ProductID, NULL);

	// Send a Feature Report to the device
	buf[0] = 0x2; // First byte is report number
	buf[1] = 0xa0;
	buf[2] = 0x0a;
	res = hid_send_feature_report(handle, buf, 17);

	// Read a Feature Report from the device
	buf[0] = 0x2;
	res = hid_get_feature_report(handle, buf, sizeof(buf));

#ifdef _DEBUG_EN_
	// Print out the returned buffer.
	printf("Feature Report\n   ");
	for (i = 0; i < res; i++)
		printf("%02hhx ", buf[i]);
	printf("\n");
#endif
	// Set the hid_read() function to be non-blocking.
	hid_set_nonblocking(handle, 1);


while(1U)
{
	uint32_t duty_cycle = 0U;
	printf("\nEnter number: ");
	scanf("%u", &duty_cycle);

	usb_t_packet = (usb_packet_t * )usb_t_buffer;
	usb_t_packet->usb_packet_u.usb_packet_s.headerid_u.headerid_s.header = 0x7E;
	usb_t_packet->usb_packet_u.usb_packet_s.headerid_u.headerid_s.len = 1U;
	usb_t_packet->usb_packet_u.usb_packet_s.duty_cycle = duty_cycle;

// print_ar(usb_t_packet->usb_packet_u.packet, 20U);

	usb_t_buffer_p = usb_t_packet->usb_packet_u.packet;
	// printf("\nPacket_sent");
	hid_write(handle, usb_t_buffer_p, HID_TRANSMIT_DATA_SIZE);
	is_packet_expected = TRUE;

	while(is_packet_expected)
	{
		usleep(5000);
		memset(usb_r_buffer, 0x0, HID_RECIEVE_DATA_SIZE);
		res = hid_read(handle, usb_r_buffer, HID_RECIEVE_DATA_SIZE);

		if (res < 0)
		{
			printf("Unable to read()\n");
			exit(0);
		}	
		else
		{
			uint8_t * usb_r_string = NULL;
			usb_r_packet = (usb_packet_t *)(usb_r_buffer);

			#ifdef _DEBUG_EN_
			printf("\n\t %x \t", usb_r_packet->usb_packet_u.usb_packet_s.headerid_u.headerid_s.header);
			#endif

			if(0x7E == usb_r_packet->usb_packet_u.usb_packet_s.headerid_u.headerid_s.header)
			{
				if(4U == usb_r_packet->usb_packet_u.usb_packet_s.headerid_u.headerid_s.len)
				{
					usb_r_string = usb_r_packet->usb_packet_u.usb_packet_s.data;			
					printf("\n%u", usb_r_packet->usb_packet_u.usb_packet_s.duty_cycle);					
				}
				else
				{
					// printf("\nPacket received");
					usb_r_string = usb_r_packet->usb_packet_u.usb_packet_s.data;			
					printf("\n%s", usb_r_string);
				}
			}

			is_packet_expected = FALSE;
		}
	}

}

// while(1);
// 	while(1)
// 	{	
// 		memset(buf, 0x0, HID_RECIEVE_DATA_SIZE);
// 		res = hid_read(handle, buf, HID_RECIEVE_DATA_SIZE);

// 		if (res < 0)
// 		{
// 			printf("Unable to read()\n");
// 			exit(0);
// 		}	
// 		else
// 		{
// 			uint8_t * usb_r_string = NULL;
// 			usb_r_packet = (usb_packet_t *)(buf);

// 			#ifdef _DEBUG_EN_
// 			printf("\n\t %x \t", usb_r_packet->usb_packet_u.usb_packet_s.headerid_u.headerid_s.header);
// 			#endif

// 			if(0x7E == usb_r_packet->usb_packet_u.usb_packet_s.headerid_u.headerid_s.header)
// 			{
// 				usb_r_string = usb_r_packet->usb_packet_u.usb_packet_s.data;			
// 				printf("\n%s", usb_r_string);
// 			}


// 			// tame.timestamp_u.timestamp_s.timestamp_ar[0] = buf[0];
// 			// tame.timestamp_u.timestamp_s.timestamp_ar[1] = buf[1];
// 			// tame.timestamp_u.timestamp_s.timestamp_ar[2] = buf[2];
// 			// tame.timestamp_u.timestamp_s.timestamp_ar[3] = buf[3];
// 			// printf("\nTimestamp%u\n",tame.timestamp_u.timestamp );
// 			// printf("\n%u \t %u",buf[4], buf[5]);

// // #ifdef PRINT_RAW_BUFFER
// // 			int counter;
// // 			for(counter = 0U; counter < HID_RECIEVE_DATA_SIZE; counter += 2U)
// // 			{
// // 					printf("%u %u ", buf[counter] , buf[counter + 1U]);	
// // 					printf("\n");					
// // 			}		
// // #endif /* PRINT_RAW_BUFFER */

// #ifdef _DEBUG_EN_
// 			printf("\nSent");
// #endif
// 			hid_write(handle, usb_hid_transmit_buffer, HID_TRANSMIT_DATA_SIZE);

// 			curr_tick = current_timestamp();
// 			diff = curr_tick - last_tick;
// 			last_tick = curr_tick;

// #ifdef _DEBUG_EN_
// 			printf("\nRed Freq host:" "%"PRIu64"\n", diff);
// #endif

// 			usleep(1000);
// 		}
// 	}




	return 0;
}





long long current_timestamp() 
{
    struct timeval te; 
    gettimeofday(&te, NULL); // get current time
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // calculate milliseconds
    // printf("milliseconds: %lld\n", milliseconds);
    return milliseconds;
}