/*Evelyn Readme*/

Board used: STM32F407VGTx Discovery @ 168MHz.
What all things are there?

on STM32 Discovery
USB_HID - USB HID class High level libraries, 
					which folows strict standards and developed carefully, 
					and libraries used for the same are provided by ST. 

cBuffer - simple Circular buffer implementation, 
					written from scratch.

clock_init - written from scratch at low level with register writes.
						 clock init for the controller,
						 initial clocl src is internal oscillator
						 enable external oscillator, feed it to PLL
						 turn off pll, configure pll, turn on PLL.

systick_init - written from scratch.
								initializes systick with 1ms period,
								uses blinky() callback to generate pwm for led.

blinky- written from scratch.
				uses systick callback.

/********************************************************************************************************************************************************/

Important files:
	evelyn .h & .c 										@breif: drivers code. holds data structure, definitions for  clock_init, systick_init, low_level_init
	evelynconfig .h & .c 							@breif: holds confiG parameters for drivers
	main .h & .c 											@breif: appliation code, blinky to generate required PWM using systick, usb send & receive functions and callbacks													
	cbuffer .h & .c 									@breif: lightweight implemntation of cbuffer. 
	common_types.h 										@breif: common types included in all files
	usbd_custom_hid_if .h & .c 				@breif: holds custom USB hid report descriptor for INPUT and OUTPUT via USB.

	compilation on linux: requires STM32 AC6 Eclipse based toolchain.
	HIDAPI installed on the linux machine which is to communiate with the board(link below).

	c_hidtest.sh: compiles the code on linux machine, provided HIDAPI libraries in shared folder of the OS.

/********************************************************************************************************************************************************/


	USB Fast Speed with HID Class on STM32-Discovery
		For all communication with Linux PC there's USB Human Interface Device Class which is used.
		USB has standard high level libraries, 
		TO use USB HID there are dependencies on STM32 HAL driver files mainly, GPIO, PWR, RCC, DMA etc. which are used only in USB's files only.

	USB HIDAPI on Linux machine
		Prewritten libraries, which allows user to send receive data over USB using HID class.
		link: https://github.com/signal11/hidapi

	systick_init
		Init code written from scratch at low level.
		systick for delay and blinking led with required duty cycle.

	clock_init
		Init code written from scratch at low level.
		initially when controller startes, Internal source(HSI) is used as core clock which equals 16MHz.
		Turn HSE and feed it to PLL and configure PLL to give 168MHz to system core clock and 48MHz to USB.

	Blinky
		Uses systick's callback to write a pwm to any particular pin, configurable via config.
		takes data from circuler buffer if its not empty and writes the PWM when the time is right.

	cBuffer
		Cbuffer implementation, very simple.

